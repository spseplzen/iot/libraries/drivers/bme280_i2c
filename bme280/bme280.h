/*
 * File:   bme280.h
 * Author: Miroslav Soukup
 * Description: Header file of bme280 temperature, pressure and humidity sensor driver.
 * 
 */



#ifndef BME280_H // Protection against multiple inclusion
#define BME280_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define BME280_I2C_CLIENT_ADDRESS_SDO0              UINT8_C(0x76)
#define BME280_I2C_CLIENT_ADDRESS_SDO1              UINT8_C(0x77)
    
#define BME280_DEVICE_ID                            UINT8_C(0x60)

#define BME280_REG_HUM_LSB                          UINT8_C(0xFE)
#define BME280_REG_HUM_MSB                          UINT8_C(0xFD)
#define BME280_REG_TEMP_XLSB                        UINT8_C(0xFC)
#define BME280_REG_TEMP_LSB                         UINT8_C(0xFB)
#define BME280_REG_TEMP_MSB                         UINT8_C(0xFA)
#define BME280_REG_PRESS_XLSB                       UINT8_C(0xF9)
#define BME280_REG_PRESS_LSB                        UINT8_C(0xF8)
#define BME280_REG_PRESS_MSB                        UINT8_C(0xF7)
#define BME280_REG_CONFIG                           UINT8_C(0xF5)
#define BME280_REG_CTRL_MEAS                        UINT8_C(0xF4)
#define BME280_REG_STATUS                           UINT8_C(0xF3)
#define BME280_REG_CTRL_HUM                         UINT8_C(0xF2)
#define BME280_REG_CALIB_HUM_41                     UINT8_C(0xF0)
#define BME280_REG_CALIB_HUM_40                     UINT8_C(0xEF)
#define BME280_REG_CALIB_HUM_39                     UINT8_C(0xEE)
#define BME280_REG_CALIB_HUM_38                     UINT8_C(0xED)
#define BME280_REG_CALIB_HUM_37                     UINT8_C(0xEC)
#define BME280_REG_CALIB_HUM_36                     UINT8_C(0xEB)
#define BME280_REG_CALIB_HUM_35                     UINT8_C(0xEA)
#define BME280_REG_CALIB_HUM_34                     UINT8_C(0xE9)
#define BME280_REG_CALIB_HUM_33                     UINT8_C(0xE8)
#define BME280_REG_CALIB_HUM_32                     UINT8_C(0xE7)
#define BME280_REG_CALIB_HUM_31                     UINT8_C(0xE6)
#define BME280_REG_CALIB_HUM_30                     UINT8_C(0xE5)
#define BME280_REG_CALIB_HUM_29                     UINT8_C(0xE4)
#define BME280_REG_CALIB_HUM_28                     UINT8_C(0xE3)
#define BME280_REG_CALIB_HUM_27                     UINT8_C(0xE2)
#define BME280_REG_CALIB_HUM_26                     UINT8_C(0xE1)
#define BME280_REG_RESET                            UINT8_C(0xE0)
#define BME280_REG_ID                               UINT8_C(0xD0)
#define BME280_REG_CALIB_TEMP_PRESS_25              UINT8_C(0xA1)
#define BME280_REG_CALIB_TEMP_PRESS_24              UINT8_C(0xA0)
#define BME280_REG_CALIB_TEMP_PRESS_23              UINT8_C(0x9F)
#define BME280_REG_CALIB_TEMP_PRESS_22              UINT8_C(0x9E)
#define BME280_REG_CALIB_TEMP_PRESS_21              UINT8_C(0x9D)
#define BME280_REG_CALIB_TEMP_PRESS_20              UINT8_C(0x9C)
#define BME280_REG_CALIB_TEMP_PRESS_19              UINT8_C(0x9B)
#define BME280_REG_CALIB_TEMP_PRESS_18              UINT8_C(0x9A)
#define BME280_REG_CALIB_TEMP_PRESS_17              UINT8_C(0x99)
#define BME280_REG_CALIB_TEMP_PRESS_16              UINT8_C(0x98)
#define BME280_REG_CALIB_TEMP_PRESS_15              UINT8_C(0x97)
#define BME280_REG_CALIB_TEMP_PRESS_14              UINT8_C(0x96)
#define BME280_REG_CALIB_TEMP_PRESS_13              UINT8_C(0x95)
#define BME280_REG_CALIB_TEMP_PRESS_12              UINT8_C(0x94)
#define BME280_REG_CALIB_TEMP_PRESS_11              UINT8_C(0x93)
#define BME280_REG_CALIB_TEMP_PRESS_10              UINT8_C(0x92)
#define BME280_REG_CALIB_TEMP_PRESS_09              UINT8_C(0x91)
#define BME280_REG_CALIB_TEMP_PRESS_08              UINT8_C(0x90)
#define BME280_REG_CALIB_TEMP_PRESS_07              UINT8_C(0x8F)
#define BME280_REG_CALIB_TEMP_PRESS_06              UINT8_C(0x8E)
#define BME280_REG_CALIB_TEMP_PRESS_05              UINT8_C(0x8D)
#define BME280_REG_CALIB_TEMP_PRESS_04              UINT8_C(0x8C)
#define BME280_REG_CALIB_TEMP_PRESS_03              UINT8_C(0x8B)
#define BME280_REG_CALIB_TEMP_PRESS_02              UINT8_C(0x8A)
#define BME280_REG_CALIB_TEMP_PRESS_01              UINT8_C(0x89)
#define BME280_REG_CALIB_TEMP_PRESS_00              UINT8_C(0x88)
    
#define BME280_CMD_SOFT_RESET                       UINT8_C(0xB6)
    

#define BME280_INT_ASSERTED_MEASURING               UINT8_C(0x01)
#define BME280_INT_ASSERTED_IM_UPDATE               UINT8_C(0x08)


#define BME280_LIMIT_TEMPERATURE_MIN                INT8_C(-40)
#define BME280_LIMIT_TEMPERATURE_MAX                INT8_C(+85)

#define BME280_LIMIT_PRESSURE_MIN                   UINT32_C(30000)
#define BME280_LIMIT_PRESSURE_MAX                   UINT32_C(110000)

#define BME280_LIMIT_HUMUDITY_MIN                   UINT8_C(0)
#define BME280_LIMIT_HUMUDITY_MAX                   UINT8_C(100)
    


// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct bme280_descriptor bme280_t;
typedef struct bme280_data_descriptor bme280_data_t;
typedef struct bme280_data_uncompensated_descriptor bme280_data_uncompensated_t;
typedef struct bme280_data_calibration_descriptor bme280_data_calibration_t;
typedef struct bme280_config_descriptor bme280_config_t;
typedef struct bme280_status_descriptor bme280_status_t;

typedef uint8_t (*bme280_i2c_funcptr_t)(uint16_t client_address, uint8_t *data, uint8_t size);
typedef void (*bme280_delay_us_funcptr_t)(uint32_t us);


typedef enum{
    BME280_CONFIG_CTRL_OVERSAMPLING_SKIPPED = 0b000,
    BME280_CONFIG_CTRL_OVERSAMPLING_1X      = 0b001,
    BME280_CONFIG_CTRL_OVERSAMPLING_2X      = 0b010,
    BME280_CONFIG_CTRL_OVERSAMPLING_4X      = 0b011,
    BME280_CONFIG_CTRL_OVERSAMPLING_8X      = 0b100,
    BME280_CONFIG_CTRL_OVERSAMPLING_16X     = 0b101
} BME280_CONFIG_CTRL_OVERSAMPLING_e;

typedef enum{
    BME280_CONFIG_MEAS_MODE_SLEEP  = 0b00,
    BME280_CONFIG_MEAS_MODE_FORCED = 0b01,
    BME280_CONFIG_MEAS_MODE_NORMAL = 0b11
} BME280_CONFIG_MEAS_MODE_e;

typedef enum{
    BME280_CONFIG_NM_INACTIVE_DURATION_0_5MS  = 0b000,
    BME280_CONFIG_NM_INACTIVE_DURATION_62_5MS = 0b001,
    BME280_CONFIG_NM_INACTIVE_DURATION_125MS  = 0b010,
    BME280_CONFIG_NM_INACTIVE_DURATION_250MS  = 0b011,
    BME280_CONFIG_NM_INACTIVE_DURATION_500MS  = 0b100,
    BME280_CONFIG_NM_INACTIVE_DURATION_1000MS = 0b101,
    BME280_CONFIG_NM_INACTIVE_DURATION_10MS   = 0b110,
    BME280_CONFIG_NM_INACTIVE_DURATION_20MS   = 0b111
} BME280_CONFIG_NM_INACTIVE_DURATION_e;

typedef enum{
    BME280_CONFIG_FILTER_OFF = 0b000,
    BME280_CONFIG_FILTER_2   = 0b001,
    BME280_CONFIG_FILTER_4   = 0b010,
    BME280_CONFIG_FILTER_8   = 0b011,
    BME280_CONFIG_FILTER_16  = 0b100
} BME280_CONFIG_FILTER_e;


struct bme280_config_descriptor{
    BME280_CONFIG_CTRL_OVERSAMPLING_e ctrl_press_oversampling;
    BME280_CONFIG_CTRL_OVERSAMPLING_e ctrl_temp_oversampling;
    BME280_CONFIG_CTRL_OVERSAMPLING_e ctrl_hum_oversampling;
    
    BME280_CONFIG_MEAS_MODE_e meas_mode;
    
    BME280_CONFIG_NM_INACTIVE_DURATION_e nm_inactive_duration;
    
    BME280_CONFIG_FILTER_e filter;
};

struct bme280_status_descriptor{
    uint8_t measuring;
    uint8_t im_update;
};

struct bme280_data_calibration_descriptor{
    uint16_t dig_t1; // calibration coefficient for the temperature sensor
    int16_t dig_t2;  // calibration coefficient for the temperature sensor
    int16_t dig_t3;  // calibration coefficient for the temperature sensor
    
    uint16_t dig_p1; // calibration coefficient for the pressure sensor
    int16_t dig_p2;  // calibration coefficient for the pressure sensor
    int16_t dig_p3;  // calibration coefficient for the pressure sensor
    int16_t dig_p4;  // calibration coefficient for the pressure sensor
    int16_t dig_p5;  // calibration coefficient for the pressure sensor
    int16_t dig_p6;  // calibration coefficient for the pressure sensor
    int16_t dig_p7;  // calibration coefficient for the pressure sensor
    int16_t dig_p8;  // calibration coefficient for the pressure sensor
    int16_t dig_p9;  // calibration coefficient for the pressure sensor
    
    uint8_t dig_h1; // calibration coefficient for the humidity sensor
    int16_t dig_h2; // calibration coefficient for the humidity sensor
    uint8_t dig_h3; // calibration coefficient for the humidity sensor
    int16_t dig_h4; // calibration coefficient for the humidity sensor
    int16_t dig_h5; // calibration coefficient for the humidity sensor
    int8_t dig_h6;  // calibration coefficient for the humidity sensor
    
    int32_t temp_fine; // intermediate temperature coefficient
};

struct bme280_data_descriptor{
    float temperature;
    float pressure;
    float humidity;
    uint8_t device_id;
};

struct bme280_data_uncompensated_descriptor{
    uint32_t temperature_raw;
    uint32_t pressure_raw;
    uint32_t humidity_raw;
};

struct bme280_descriptor{
    uint16_t client_address;
    
    bme280_config_t config;
    
    bme280_status_t status;
    
    bme280_i2c_funcptr_t i2c_write;
    bme280_i2c_funcptr_t i2c_read;
    bme280_delay_us_funcptr_t delay_us;
    
    bme280_data_t data;
    bme280_data_uncompensated_t data_uncompensated;
    bme280_data_calibration_t data_calibration;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization bme280 sensor.
 *
 * \param me pointer to bme280 sensor type of bme280_t
 * \param config pointer to bme280 configuration type of bme280_config_t
 *
 * \returns status of success / failure
 */
uint8_t bme280_init (bme280_t *me, uint16_t client_address, bme280_config_t *config);

uint8_t bme280_i2c_write_register (bme280_t *me, bme280_i2c_funcptr_t i2c_write_funcptr);

uint8_t bme280_i2c_read_register (bme280_t *me, bme280_i2c_funcptr_t i2c_read_funcptr);

uint8_t bme280_delay_us_register (bme280_t *me, bme280_delay_us_funcptr_t delay_us_funcptr);

uint8_t bme280_get_data (bme280_t *me);

uint8_t bme280_i2c_check (bme280_t *me);

uint8_t bme280_set_power_mode (bme280_t *me, BME280_CONFIG_MEAS_MODE_e meas_mode);

uint8_t bme280_get_device_id (bme280_t *me);

uint8_t bme280_get_device_status (bme280_t *me);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of BME280_H
