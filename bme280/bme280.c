/*
 * File:   bme280.c
 * Author: Miroslav Soukup
 * Description: Source file of bme280 temperature, pressure and humidity sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "bme280.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _bme280_write_reg(bme280_t *me, uint8_t reg, uint8_t data){
    uint8_t _data[2] = {reg, data};
    if(!me->i2c_write(me->client_address, _data, 2)) return 0;
    return 1;
}

static uint8_t _bme280_read_reg(bme280_t *me, uint8_t reg, uint8_t *data){
    if(!me->i2c_write(me->client_address, &reg, 1)) return 0;
    if(!me->i2c_read(me->client_address, data, 1)) return 0;
    return 1;
}

uint8_t bme280_init (bme280_t *me, uint16_t client_address, bme280_config_t *config) {
    me->config = *config;
    me->client_address = client_address;
    
    
    /* ************************ DEVICE ID ************************ */
    if(!bme280_get_device_id(me)) return 0;
    if(me->data.device_id != BME280_DEVICE_ID) return 0;
    /* *********************************************************** */
    
    /* ************************** RESET ************************** */
    if(!_bme280_write_reg(me, BME280_REG_RESET, BME280_CMD_SOFT_RESET)) return 0;
    /* *********************************************************** */

    /* **************** SET STANDBY MODE ************************* */
    uint8_t ctrl_meas = me->config.ctrl_temp_oversampling << 5 |  me->config.ctrl_press_oversampling << 2 | BME280_CONFIG_MEAS_MODE_SLEEP;
    if(!_bme280_write_reg(me, BME280_REG_CTRL_MEAS, ctrl_meas)) return 0;
    /* *********************************************************** */

    /* ********************* HUM OVS CONFIG ********************** */
    if(!_bme280_write_reg(me, BME280_REG_CTRL_HUM, me->config.ctrl_hum_oversampling)) return 0;
    /* *********************************************************** */

    /* ********************* IA DUR FILTER *********************** */
    if(!_bme280_write_reg(me, BME280_REG_CONFIG, me->config.nm_inactive_duration << 5 | me->config.filter << 2)) return 0;
    /* *********************************************************** */

    /* ********************* GET CALIB DATA ********************** */
    uint8_t _calib_temp_press[26];
    for(uint8_t i = 0; i < 26; ++i)
        if(!_bme280_read_reg(me, BME280_REG_CALIB_TEMP_PRESS_00 + i, &_calib_temp_press[i])) return 0;
    
    me->data_calibration.dig_t1 = _calib_temp_press[1] << 8 | _calib_temp_press[0];
    me->data_calibration.dig_t2 = (int16_t) (_calib_temp_press[3] << 8 | _calib_temp_press[2]);
    me->data_calibration.dig_t3 = (int16_t) (_calib_temp_press[5] << 8 | _calib_temp_press[4]);
    me->data_calibration.dig_p1 = _calib_temp_press[7] << 8 | _calib_temp_press[6];
    me->data_calibration.dig_p2 = (int16_t) (_calib_temp_press[9] << 8 | _calib_temp_press[8]);
    me->data_calibration.dig_p3 = (int16_t) (_calib_temp_press[11] << 8 | _calib_temp_press[10]);
    me->data_calibration.dig_p4 = (int16_t) (_calib_temp_press[13] << 8 | _calib_temp_press[12]);
    me->data_calibration.dig_p5 = (int16_t) (_calib_temp_press[15] << 8 | _calib_temp_press[14]);
    me->data_calibration.dig_p6 = (int16_t) (_calib_temp_press[17] << 8 | _calib_temp_press[16]);
    me->data_calibration.dig_p7 = (int16_t) (_calib_temp_press[19] << 8 | _calib_temp_press[18]);
    me->data_calibration.dig_p8 = (int16_t) (_calib_temp_press[21] << 8 | _calib_temp_press[20]);
    me->data_calibration.dig_p9 = (int16_t) (_calib_temp_press[23] << 8 | _calib_temp_press[22]);
    me->data_calibration.dig_h1 = _calib_temp_press[25];
    
    
    uint8_t _calib_hum[7];
    for(uint8_t i = 0; i < 7; ++i)
        if(!_bme280_read_reg(me, BME280_REG_CALIB_HUM_26 + i, &_calib_hum[i])) return 0;
    
    me->data_calibration.dig_h2 = (int16_t) (_calib_hum[1] << 8 | _calib_hum[0]);
    me->data_calibration.dig_h3 = _calib_hum[2];
    int16_t dig_h4_msb = (int16_t)(int8_t)_calib_hum[3] * 16;
    int16_t dig_h4_lsb = (int16_t)(_calib_hum[4] & 0x0F);
    me->data_calibration.dig_h4 = dig_h4_msb | dig_h4_lsb;
    int16_t dig_h5_msb = (int16_t)(int8_t)_calib_hum[5] * 16;
    int16_t dig_h5_lsb = (int16_t)(_calib_hum[4] >> 4);
    me->data_calibration.dig_h5 = dig_h5_msb | dig_h5_lsb;
    me->data_calibration.dig_h6 = (int8_t)_calib_hum[6];
    /* *********************************************************** */

    /* *************** SET MEASURE MODE VIA CONFIG *************** */
    ctrl_meas = (ctrl_meas & 0b11111100) | me->config.meas_mode;
    if(!_bme280_write_reg(me, BME280_REG_CTRL_MEAS, ctrl_meas)) return 0;
    /* *********************************************************** */
    
    return 1;
}

uint8_t bme280_get_data(bme280_t *me) {
    
    uint8_t _data[3];
    if(!_bme280_read_reg(me, BME280_REG_TEMP_MSB, &_data[0])) return 0;
    if(!_bme280_read_reg(me, BME280_REG_TEMP_XLSB, &_data[1])) return 0;
    if(!_bme280_read_reg(me, BME280_REG_TEMP_LSB, &_data[2])) return 0;
    
    me->data_uncompensated.temperature_raw = _data[0] << 12 | _data[1] << 4 | _data[2] >> 4; 
    
    double var1 = (((double) me->data_uncompensated.temperature_raw) / 16384.0 - ((double) me->data_calibration.dig_t1) / 1024.0) * ((double) me->data_calibration.dig_t2);
    double var2 = (((double) me->data_uncompensated.temperature_raw) / 131072.0 - ((double) me->data_calibration.dig_t1) / 8192.0) ;
    var2 = (var2 * var2) * ((double) me->data_calibration.dig_t3);
    me->data_calibration.temp_fine = (int32_t)(var1 + var2);
    me->data.temperature = (me->data_calibration.temp_fine) / 5120.0;

    if(me->data.temperature < BME280_LIMIT_TEMPERATURE_MIN) me->data.temperature = BME280_LIMIT_TEMPERATURE_MIN;
    else if(me->data.temperature > BME280_LIMIT_TEMPERATURE_MAX) me->data.temperature = BME280_LIMIT_TEMPERATURE_MAX;
    
    
    
    if(!_bme280_read_reg(me, BME280_REG_PRESS_MSB, &_data[0])) return 0;
    if(!_bme280_read_reg(me, BME280_REG_PRESS_XLSB, &_data[1])) return 0;
    if(!_bme280_read_reg(me, BME280_REG_PRESS_LSB, &_data[2])) return 0;
    
    me->data_uncompensated.pressure_raw = _data[0] << 12 | _data[1] << 4 | _data[2] >> 4;

    var1 = ((double) me->data_calibration.temp_fine / 2.0) - 64000.0;
    var2 = var1 * var1 * ((double) me->data_calibration.dig_p6) / 32768.0;
    var2 = var2 + var1 * ((double) me->data_calibration.dig_p5) * 2.0;
    var2 = (var2 / 4.0) + (((double) me->data_calibration.dig_p4) * 65536.0);
    var1 = ((((double) me->data_calibration.dig_p3) * var1 * var1 / 524288.0) + ((double) me->data_calibration.dig_p2) * var1) / 524288.0;
    var1 = (1.0 + var1 / 32768.0) * ((double) me->data_calibration.dig_p1);

    if(var1 > 0){
        me->data.pressure = 1048576.0 - (double) me->data_uncompensated.pressure_raw;
        me->data.pressure = (me->data.pressure - (var2 / 4096.0)) * 6250.0 / var1;
        var1 = ((double) me->data_calibration.dig_p9) * me->data.pressure * me->data.pressure / 2147483648.0;
        var2 = me->data.pressure * ((double) me->data_calibration.dig_p8) / 32768.0;
        me->data.pressure = me->data.pressure + (var1 + var2 + ((double) me->data_calibration.dig_p7)) / 16.0;

        if(me->data.pressure < BME280_LIMIT_PRESSURE_MIN) me->data.pressure = BME280_LIMIT_PRESSURE_MIN;
        else if(me->data.pressure > BME280_LIMIT_PRESSURE_MAX) me->data.pressure = BME280_LIMIT_PRESSURE_MAX;

    }
    else{
        me->data.pressure = BME280_LIMIT_PRESSURE_MIN;
    }
    
    
    
    if(!_bme280_read_reg(me, BME280_REG_HUM_MSB, &_data[0])) return 0;
    if(!_bme280_read_reg(me, BME280_REG_HUM_LSB, &_data[1])) return 0;
    
    me->data_uncompensated.humidity_raw = _data[0] << 8 | _data[1];
    
    var1 = ((double) me->data_calibration.temp_fine) - 76800.0;
    var2 = (((double) me->data_calibration.dig_h4) * 64.0 + (((double) me->data_calibration.dig_h5) / 16384.0) * var1);
    double var3 = me->data_uncompensated.humidity_raw - var2;
    double var4 = ((double) me->data_calibration.dig_h2) / 65536.0;
    double var5 = (1.0 + (((double) me->data_calibration.dig_h3) / 67108864.0) * var1);
    double var6 = 1.0 + (((double) me->data_calibration.dig_h6) / 67108864.0) * var1 * var5;
    var6 = var3 * var4 * (var5 * var6);
    me->data.humidity = var6 * (1.0 - ((double) me->data_calibration.dig_h1) * var6 / 524288.0);
    
    if (me->data.humidity > BME280_LIMIT_HUMUDITY_MAX) me->data.humidity = BME280_LIMIT_HUMUDITY_MAX;
    else if(me->data.humidity < BME280_LIMIT_HUMUDITY_MIN) me->data.humidity = BME280_LIMIT_HUMUDITY_MIN;
    
    return 1;
}

uint8_t bme280_i2c_write_register (bme280_t *me, bme280_i2c_funcptr_t i2c_write_funcptr){
    if(!i2c_write_funcptr) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

uint8_t bme280_i2c_read_register (bme280_t *me, bme280_i2c_funcptr_t i2c_read_funcptr){
    if(!i2c_read_funcptr) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t bme280_delay_us_register (bme280_t *me, bme280_delay_us_funcptr_t delay_us_funcptr){
    if(!delay_us_funcptr) return 0;
    me->delay_us = delay_us_funcptr;
    return 1;
}

uint8_t bme280_i2c_check (bme280_t *me){
    uint8_t _byte;
    return me->i2c_read(me->client_address, &_byte, 1);
}

uint8_t bme280_set_power_mode (bme280_t *me, BME280_CONFIG_MEAS_MODE_e power_mode){
    // TO BE DONE
    return 1;
}

uint8_t bme280_get_device_id (bme280_t *me){
    uint8_t dev_id;
    if(!_bme280_read_reg(me, BME280_REG_ID, &dev_id)) return 0;
    me->data.device_id = dev_id;
    return 1;
}

uint8_t bme280_get_device_status (bme280_t *me){
    uint8_t _byte;
    if(!_bme280_read_reg(me, BME280_REG_STATUS, &_byte)) return 0;
    me->status.measuring = (_byte & BME280_INT_ASSERTED_MEASURING) ? 1 : 0;
    me->status.im_update = (_byte & BME280_INT_ASSERTED_IM_UPDATE) ? 1 : 0;
    return 1;
}
