#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "bme280/bme280.h"


void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}

void delay_us(uint32_t us){
    SYSTICK_TimerStart();
    SYSTICK_DelayUs(us);
    SYSTICK_TimerStop();
}

uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Write(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Read(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}


int main(void)
{
    SYS_Initialize(NULL);
    
    
    bme280_t bme280 = {};
    
    bme280_i2c_read_register(&bme280, i2c_read);
    bme280_i2c_write_register(&bme280, i2c_write);
    bme280_delay_us_register(&bme280, delay_us);
    
    
    bme280_config_t config = {};
    config.ctrl_press_oversampling = BME280_CONFIG_CTRL_OVERSAMPLING_1X;
    config.ctrl_temp_oversampling  = BME280_CONFIG_CTRL_OVERSAMPLING_1X;
    config.ctrl_hum_oversampling   = BME280_CONFIG_CTRL_OVERSAMPLING_1X;
    config.meas_mode = BME280_CONFIG_MEAS_MODE_NORMAL;
    config.nm_inactive_duration = BME280_CONFIG_NM_INACTIVE_DURATION_250MS;
    config.filter = BME280_CONFIG_FILTER_2;
    
    if(!bme280_init(&bme280, BME280_I2C_CLIENT_ADDRESS_SDO0, &config)){
        printf("BME280 INITIALIZATION FAILED\r\n");
    }
    
    
    while(true)
    {
        SYS_Tasks();
        
        
        if(!bme280_get_device_status(&bme280)) printf("I2C ERROR\r\n");

        if(!bme280.status.measuring){
            
            if(!bme280_get_data(&bme280) && !bme280_get_device_id(&bme280)){
                printf("I2C ERROR\r\n");
            }
            else{
                printf("device id = 0x%02X\r\n", bme280.data.device_id);
                printf("RAW: temperature = %ld, pressure = %ld, humidity = %ld\r\n", bme280.data_uncompensated.temperature_raw, bme280.data_uncompensated.pressure_raw, bme280.data_uncompensated.humidity_raw);
                printf("temperature = %.04f, pressure = %.02f, humidity = %.02f\r\n", bme280.data.temperature, bme280.data.pressure, bme280.data.humidity);
            }
        }
        
        delay_ms(500);
        
    }
    
    return (EXIT_FAILURE);
}
